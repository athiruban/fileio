class StaticPagesController < ApplicationController
  # 
  # Static home page
  #
  def home
  end

  # 
  # Static demo page
  #
  def demo
  end
  
  # 
  # Static about page
  #
  def about
  end
end
