class SessionsController < ApplicationController
	#
	# Get the page for user login which will be forwarded to 'create' as post action
	#
	def new
	end

	#
	# Receive the params and check user type if not default go to the appropriate page else display the 
	# message that your account will be activated by the admin shortly
	#
	def create
	  puts "params received: #{params}" 	
	  @user = User.find_by(:email => params['session']['email'])

	  if @user.present?
	  	if @user.activated? && @user.authenticate(params['session']['password'])
	  		login(@user)
	  		set_current_folder_as_home
	  		redirect_to home_path
	  	else
	  		flash[:info] = "Your registration is under approval by Admin. We'll send your account activation status thru email."
	  		render 'new'
	  	end
	  else
	  	flash[:warning] = "Login failed. Bad Username/Password"
	  	render 'new'
	  end
	end

	#
	# should destroy user session and clear all cache
	#
  def destroy
  	logout
    flash[:info] = 'You have successfully signed out!'
    redirect_to root_url
  end

	private
		#
		# Strong type parameters
		#
		def session_params
			params.require('session').permit('email', 'password')
		end
end
