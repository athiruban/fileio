class DataFilesController < ApplicationController
  def index
  # list all the files

  end

  def new
  # show the file upload page
    @data_file = DataFile.new
  end

  def create
  # receive the POST data and create entry in database

  end

  def show
  # user should able to receive the file

  end

  def destroy
  # user should able to remove the file
  
  end   

  def upload
    DataFile.upload(params[:file])
    redirect_to root_url, notice: "File uploaded"
  end
end
