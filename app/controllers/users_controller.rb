class UsersController < ApplicationController
	# Filters before, after or around actions, order of declaration matters the most!

	def show
		@user = User.find_by(:id => params[:id])
	end

	#
	# should return a reference to user object so that we can associate error messages to it.
	#
	def new
		@user = User.new
	end

	#
	# should accept user details and create necessary entries in db
	#
	def create
		@user = User.new(user_params.merge({:admin => 'N'}))
		if @user.save
			log_in @user
			#flash[:notice] = "You account have been registered successfully!"
			redirect_to home_path
		else
			flash[:notice] = "Please correct the errors and try again!"
			render 'new'
		end	
	end

	private

	#
	# strong type parameters for user object
	#
	def user_params
		params.require(:user).permit(:name, :email, :password, :password_confirmation)
	end

end
