class Group < ActiveRecord::Base
  # need to track the user who created a group 
  belongs_to :user
  validates_associated :user
  # other validations
  validates :group_name, :user_id, :presence => true
  # Todo
  # rename column 'user_id' in this table to 'created_by'
  # has_many :user, :through => :user_group_mapping
  def created_by
    self.user_id
  end
end
