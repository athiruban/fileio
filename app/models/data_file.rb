class DataFile < ActiveRecord::Base
	# attr_accessible :file

	# belongs_to :user
	# validates_associated :user

	# belongs_to :group
	# validates_associated :group

	# validates :filename, :presence => true	

	# validates :filestatus, :inclusion => { :in => %w(A D), :message => "%{value} is not a valid file status"}
	# validates :fileformat, :inclusion => { :in => %w(txt jpg pdf doc), :message => "%{value} is not a valid file format"}

	mount_uploader :file, DataFileUploader

	before_save :set_defaults

=begin
	def self.save(upload)

		name = upload['datafile'].original_filename
		name = sanitize_filename(name)
		directory = "public/data/userfiles"

		# create the file path
		path = File.join(directory, name)
		# write tthe file
		File.open(path, "wb") { |f| f.write(upload['datafile'].read) }
	end
=end

	def set_defaults
		self.fileformat = "text"
		self.filesize   = "10 KB"
		self.user_id    = nil
		self.filestatus = "u"
		self.folder_id  = 1
    # t.string   "fileformat", limit: 100
    # t.string   "filesize",   limit: 13
    # t.integer  "user_id",    limit: 4
    # t.string   "filestatus", limit: 1
    # t.integer  "folder_id",  limit: 4
	end

	def absolute_url
		self.folder.absolute_path unless self.folder.nil?
	end

	def self.upload(file)
		raise "To do"
	end

	private

	def sanitize_filename(file_name)
		just_filename = File.basename(file_name)
		just_filename.sub(/[^\w\.\-]/, '_')
	end
end
