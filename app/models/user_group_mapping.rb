class UserGroupMapping < ActiveRecord::Base
  # validation
  validate :valid_groups?
  # callback
  before_create :expand_group_string
  # constants
  MAX_LENGTH = 2000.freeze

  belongs_to :user

  protected
    def valid_groups?
      errors[:groups] = "Invalid string present" unless self.groups.each_char.select {|c| !(c == "Y" || c == "N")}.empty?
    end

  private
    def expand_group_string
      return if self.groups.length == MAX_LENGTH
      cur_length  = self.groups.length
      pad_length  = MAX_LENGTH - cur_length
      str = self.groups
      str = (str.split('') << (' ' * pad_length).split('')).flatten
      self.update_attributes(:groups => str)
      self.reload
      
      Rails.logger.debug("\n<<<#{cur_length}, #{pad_length}, #{self.groups.length}>>>\n")
    end
end
