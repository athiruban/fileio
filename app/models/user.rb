class User < ActiveRecord::Base
	has_many :data_file
  # has_many :group, :through => :user_group_mapping

	validates :name, :email, :admin, :presence => true

	validates :email, :format => {:with => /[a-zA-Z]+@[a-zA-Z]+\.com/}
	validates :email, :uniqueness => true

	has_secure_password

	def self.new_token
	  SecureRandom.hex(20)
	end

	def self.encode_secret(string)
 	  cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
	end

  def activated?
  	"Y" == self.activated
  end

	private
    def user_group_mapping
      []
    end
end
