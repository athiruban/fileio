class Folder < ActiveRecord::Base
  # Associations
	belongs_to :group
  # Validations
  validates_associated :group

	# Self association
	belongs_to :parent, class_name: "Folder"
	validates :parent_id, :folder_name, :absolute_path, :home, :presence => true

  # Scope
  scope :root_directory, -> { where(parent_id == self.id )}
end
