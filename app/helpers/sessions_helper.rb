module SessionsHelper
	#
	# Establish user session
	#	
	def login(user)
		session[:user_id] = user.id
	end

	#
	# when called should return the active record
	#
	def current_user
		user_id = session[:user_id]
		@current_user = User.find_by(:id => user_id)
	end

	#
	# should say user logged in or not
	#
	def is_logged_in?
		!current_user.nil?
	end

	#
	# should clear the user session variables
	#
	def logout
		session.delete(:user_id)
		@current_user = nil
	end

	#
	# should return the current folder id when logged in, probably we should cache this data
	#
	def current_folder
		folder_id = session[:folder_id]
		@current_folder = Folder.find_by(:id => folder_id)
	end

	def set_current_folder_as_home
		@current_folder = Folder.first
	end
end
