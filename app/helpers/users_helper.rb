module UsersHelper
	#
	# should check whether the current folder is visible to user
	#
	def is_current_folder_accessible_to_user?		
		check_access current_user current_folder
	end	

	#
	# it fetches the group id of the folder and confirms whether user is inside or not.
	#
	def check_access(user, folder)
		folder_group_id = folder.group_id
		return false if folder_group_id.nil?

	end

	def log_in(user)
		session['user_id'] = user.id
	end

	def current_user
		User.find_by(:id => session['user_id'])
	end

	def logged_in?
		!current_user.nil?
	end
end
