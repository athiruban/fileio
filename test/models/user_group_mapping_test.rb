require 'test_helper'

class UserGroupMappingTest < ActiveSupport::TestCase
  def setup
    @mapping1 = user_group_mappings(:mapping1)
    @user1 = users(:user1)
  end  
  #
  # Happy path test case
  #
  test "map should create association between user and group" do 
    assert_not @mapping1.nil?
    assert @mapping1.valid?, "group data should be valid"
  end

  test "group string length properly expands" do 
    assert @mapping1.groups.length == 3, "Check default length"
    @mapping1.send(:expand_group_string)
    Rails.logger.debug("\n<<<Length after expansion>>> #{@mapping1.groups.length}\n")
    assert @mapping1.groups.length == 3, "groups column should expand to 2000 chars after save"
  end
  #
  # Unhappy path test case
  #
  test "group string should not contain invalid combinations" do
    local_map = UserGroupMapping.create({
      :user_id => @user1.id,
      :created_at => Time.now,
      :updated_at => Time.now,
      :groups => "XYZABC"
    }) 
    assert_not local_map.valid?, "group string should not contain chars other than 'Y' or 'N'"
  end
end
