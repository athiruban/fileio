require 'test_helper'

class UserTest < ActiveSupport::TestCase
	def setup
		@user1 = users(:user1)
		@user2 = users(:user2)
	end
	#
	# Happy path test case
	#
	test "users should have proper attributes" do 
		assert @user1.valid?			
	end
	#
	# Un-happy path test case
	#
	test "users should have all necessary attributes" do 
		local_user = User.create({
			:name => 'hacker',
			:email => "athi@sample.com"
		})
		assert_not local_user.valid?			
	end

	test "users should not have invalid email adddress" do 
		local_user = User.create({
			:name => 'hacker',
			:email => "athi@sample.hacker",
	    :password_digest => User.new_token,
	    :created_at => Time.now,
  		:updated_at => Time.now,
  		:admin => 'Y'
		})
		assert_not local_user.valid?			
	end

	test "user not activated check" do 
		assert_not @user2.activated?, "User is not activated so far by admin"
	end
end
