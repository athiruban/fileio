require 'test_helper'

class GroupTest < ActiveSupport::TestCase
  def setup
    @group1 = groups(:group1)
  end
  #
  # Happy path test case
  #
  test "valid group" do 
    assert_not @group1.nil? 
    assert @group1.valid?, "Group should be valid"
  end
  #
  # Unhappy path test case
  #
  test "invalid group" do 
    invalid_group = Group.create({
      :group_name => "",
      :created_at => Time.now,
      :updated_at => Time.now
      })
    assert_not invalid_group.valid? 
  end
end
