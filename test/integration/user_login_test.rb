require 'test_helper'

class UserLoginTest < ActionDispatch::IntegrationTest
	def setup
		@non_active_user = users(:user2)
		@active_user = users(:user1)
	end

	#
	# Happy path test case
	#
	test "valid login test" do 
		get login_path
		assert_template 'sessions/new'

		post login_path, :session => {
			:email => @active_user.email,
			:password => 'password'
		}
		assert_redirected_to home_path
		assert flash.empty?, "Flash message should not come after successfully login"

		# test the links present in the page
		# assert_template 'static_pages/home'
	  assert_select "<a href=?", new_admin_group_subscribe_url, :count => 1
	end

	#
	# Unhappy path test case
	#
	test "should redirect non active user to login page" do 
		# user is valid however he can't login unless he is activated by admin
		assert @non_active_user.valid?
		get login_path
		assert_template 'sessions/new'

		post login_path, :session => {
			:email => @non_active_user.email,
			:password => 'password'
		}
		assert_not flash.empty?, "Flash should report user not activated status"
		assert_template 'sessions/new'
	end
end
