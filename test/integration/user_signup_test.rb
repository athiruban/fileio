require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest
  #
  # Happy path test case
  #
  test "user should able to create account in fileio" do 
    get signup_path
    assert_template "users/new"

    assert_difference 'User.count', +1 do 
      post signup_path, :user => {
        :email => "someone@example.com",
        :name => "someone",
        :password => 'password',
        :password_confirmation => 'password'
      }
    end

    assert flash.empty?
    assert_redirected_to home_path
  end
  #
  # Unhappy path test case
  #
  test "user should not able to create account in fileio" do 
    get signup_path
    assert_template "users/new"

    assert_no_difference 'User.count' do 
      post signup_path, :user => {
        :email => "someone@example.com",
        :name => "",
        :password => 'password',
        :password_confirmation => 'password'
      }
    end

    assert_not flash.empty?
    assert_template "users/new"
  end
end
