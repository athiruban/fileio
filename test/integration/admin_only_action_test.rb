require 'test_helper'

class AdminOnlyActionTest < ActionDispatch::IntegrationTest
  def setup
    @admin_user = users(:admin1)
  end

  #
  # Happy path test case
  #
  test "should allow admin user to add user to group" do 
    get login_path
    assert_template 'sessions/new'

    post login_path, :session => {
      :email => @admin_user.email,
      :password => 'password'
    }
    assert_redirected_to home_path
    assert flash.empty?, "Flash message should not come after successfully login"

    # assert_select "a href=?", new_admin_group_subscribe_path
  end

  #
  # Unhappy path test case
  #
  test "should not allow oridinary user to add user to group" do 
  end
end
