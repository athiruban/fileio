require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
	test "should redirect home and root to sessions controller" do 
		get :home
		assert_response :success
		assert_template "static_pages/home", "user home should route to static_pages controller"
		assert_select "title", "Home | Fileio"
	end

	test "should redirect demo to sessions controller" do 
		get :demo
		assert_response :success
		assert_template "static_pages/demo", "demo path should route to static_pages controller"
		assert_select "title", "Demo | Fileio"
	end

	test "should redirect about to sessions controller" do 
		get :about
		assert_response :success
		assert_template "static_pages/about", "about path should route to static_pages controller"
		assert_select "title", "About | Fileio"
	end
end
