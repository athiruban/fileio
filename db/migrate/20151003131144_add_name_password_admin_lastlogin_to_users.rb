class AddNamePasswordAdminLastloginToUsers < ActiveRecord::Migration
  def change
    add_column :users, :password_digest, :text, :null => false
    add_column :users, :name, :string, :limit => 50, :null => false
    add_column :users, :email, :string, :limit => 1024, :null => false
    add_column :users, :admin, :integer, :limit => 1
    add_column :users, :last_login, :datetime
  end
end
