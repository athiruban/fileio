class AddGroupNameToGroups < ActiveRecord::Migration
  def change
  	add_column :groups, :group_name, :string, :limit => 1024, :null => false
  end
end
