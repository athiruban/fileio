class AddFolderRefToDataFiles < ActiveRecord::Migration
  def change
  	add_reference :data_files, :folder, :index => true
  end
end
