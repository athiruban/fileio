class AddDefaultValueToUsers < ActiveRecord::Migration
  def change
  	change_column_default :users, :activated, 'N' 
  end
end
