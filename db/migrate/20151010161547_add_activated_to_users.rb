class AddActivatedToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :activated, :string, :limit => 1
  end
end
