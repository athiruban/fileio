class AddBaseFolder < ActiveRecord::Migration
  def up
    execute("insert into groups (group_name, created_at, updated_at) values ('Global', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP );")
    execute("insert into folders (parent_id, folder_name, absolute_path, home, group_id, created_at, updated_at) values (0, 'Home', '/', 'N', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP); ")
  end

  def down
    execute("delete from groups where group_name = 'Global';")
  end
end
