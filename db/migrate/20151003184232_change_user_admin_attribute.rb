class ChangeUserAdminAttribute < ActiveRecord::Migration
  def change
  	reversible do |dir|
  		change_table :users do |t|
  			dir.up {t.change :admin, :string, :limit => 1}
  			dir.down {t.change :admin, :integer, :limit => 1}
  		end
  	end
  end
end
