class AddAttributesToUserGroupMappings < ActiveRecord::Migration
  def change
  	add_column :user_group_mappings, :groups, :string, :limit => 2000
  	add_reference :user_group_mappings, :user, :index => true
  end
end
