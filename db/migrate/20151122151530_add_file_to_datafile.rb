class AddFileToDatafile < ActiveRecord::Migration
  def change
    add_column :data_files, :file, :string
  end
end
