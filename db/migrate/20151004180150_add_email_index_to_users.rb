class AddEmailIndexToUsers < ActiveRecord::Migration
  def change
  	change_column :users, :email, :string, :limit => 200
  	add_index :users, :email, :unique => true
  end
end
