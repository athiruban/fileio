class AddAttributesToFolders < ActiveRecord::Migration
  def change
  	add_column :folders, :parent_id, :integer, :limit => 4, :null => false
  	add_column :folders, :folder_name, :string, :limit => 2048, :null => false
  	add_column :folders, :absolute_path, :text, :limit => 2097152, :null => false
  	add_column :folders, :home, :string, :limit => 1, :null => false
  	add_reference :folders, :group, :index => true, :null => false
  end
end
