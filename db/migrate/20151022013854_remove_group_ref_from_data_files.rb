class RemoveGroupRefFromDataFiles < ActiveRecord::Migration
  def change
  	remove_reference :data_files, :group
  end
end
