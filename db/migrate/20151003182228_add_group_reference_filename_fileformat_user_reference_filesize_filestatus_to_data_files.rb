class AddGroupReferenceFilenameFileformatUserReferenceFilesizeFilestatusToDataFiles < ActiveRecord::Migration
  def change
  	add_reference :data_files, :group, :index => true
  	add_column :data_files, :filename, :string, :limit => 2048, :null => false
  	add_column :data_files, :fileformat, :string, :limit => 100
  	add_column :data_files, :filesize, :string, :limit => 13
  	add_reference :data_files, :user, :index => true
  	add_column :data_files, :filestatus, :string, :limit => 1
  end
end
