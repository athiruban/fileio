class CreateUserGroupMappings < ActiveRecord::Migration
  def change
    create_table :user_group_mappings do |t|

      t.timestamps null: false
    end
  end
end
