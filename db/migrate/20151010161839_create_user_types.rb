class CreateUserTypes < ActiveRecord::Migration
  def change
    create_table :user_types do |t|
    	t.string :type, :limit => 1, :index => true
    	t.string :type_description
    end
  end
end
