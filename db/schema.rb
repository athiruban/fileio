# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151122151530) do

  create_table "data_files", force: :cascade do |t|
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "filename",   limit: 2048, null: false
    t.string   "fileformat", limit: 100
    t.string   "filesize",   limit: 13
    t.integer  "user_id",    limit: 4
    t.string   "filestatus", limit: 1
    t.integer  "folder_id",  limit: 4
    t.string   "file",       limit: 255
  end

  add_index "data_files", ["folder_id"], name: "index_data_files_on_folder_id", using: :btree
  add_index "data_files", ["user_id"], name: "index_data_files_on_user_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "folders", force: :cascade do |t|
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "parent_id",     limit: 4,        null: false
    t.string   "folder_name",   limit: 2048,     null: false
    t.text     "absolute_path", limit: 16777215, null: false
    t.string   "home",          limit: 1,        null: false
    t.integer  "group_id",      limit: 4,        null: false
  end

  add_index "folders", ["group_id"], name: "index_folders_on_group_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "group_name", limit: 1024, null: false
    t.integer  "user_id",    limit: 4
  end

  add_index "groups", ["user_id"], name: "index_groups_on_user_id", using: :btree

  create_table "user_group_mappings", force: :cascade do |t|
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "groups",     limit: 2000
    t.integer  "user_id",    limit: 4
  end

  add_index "user_group_mappings", ["user_id"], name: "index_user_group_mappings_on_user_id", using: :btree

  create_table "user_types", force: :cascade do |t|
    t.string "type",             limit: 1
    t.string "type_description", limit: 255
  end

  add_index "user_types", ["type"], name: "index_user_types_on_type", using: :btree

  create_table "users", force: :cascade do |t|
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.text     "password_digest", limit: 65535,               null: false
    t.string   "name",            limit: 50,                  null: false
    t.string   "email",           limit: 200,                 null: false
    t.string   "admin",           limit: 1
    t.datetime "last_login"
    t.string   "activated",       limit: 1,     default: "N"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

end
