Rails.application.routes.draw do
  root 'static_pages#home'
  
  get 'home' => 'static_pages#home'

  get 'demo' => 'static_pages#demo'

  get 'about' => 'static_pages#about'

  resources :data_files, :only => [:index, :new, :create, :show, :destroy] do 
    collection { post :upload }
  end

  resources :folders, :only => [:index, :new, :create, :edit]

  resources :users, :only => [:new, :create, :show]

  resources :sessions, :only => [:new, :create, :destroy]

  get 'profile' => 'users#show'

  get 'upload' => 'upload#index'

  get 'signup' => 'users#new'

  post 'signup' => 'users#create'

  get 'login'  => 'sessions#new'

  post 'login'  => 'sessions#create'

  get 'logout' => 'sessions#destroy'

  namespace :admin do
    resources :user_groups
  end
end
